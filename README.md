Example RequestHandler and WebSocketHandler usage:


```
#!python

import asyncio

import tornado.ioloop
import tornado.web
from tornado.httpclient import AsyncHTTPClient
from tornado.platform.asyncio import AsyncIOMainLoop

from jsonrpc.dispatcher import JsonRpcDispatcher
from jsonrpc.handlers.web import JsonRpcRequestHandler
from jsonrpc.handlers.websocket import JsonRpcWebSocketHandler
from jsonrpc.registry import MethodRegistry

registry = MethodRegistry()
dispatcher = JsonRpcDispatcher(registry)

# plain ol' synchronous rpc method example
@registry.register("echo")
def echo(*args, **kwargs):
    if args:
        return args
    elif kwargs:
        return kwargs


# async rpc method example
@registry.register("get_homepage")
async def get_homepage():
    http_client = AsyncHTTPClient()
    response = await http_client.fetch("http://tevans.us")
    return response.body


def make_app():
    return tornado.web.Application([
        # http request handler
        (r'/api', JsonRpcRequestHandler, dict(dispatcher=dispatcher)),
        # websocket handler
        (r'/ws-api', JsonRpcWebSocketHandler, dict(dispatcher=dispatcher))
    ])


if __name__ == "__main__":
    AsyncIOMainLoop().install()
    ioloop = asyncio.get_event_loop()

    app = make_app()
    app.listen(8888)
    ioloop.run_forever()
```


Example Raw Socket Handling:

```
#!python

from tornado.httpclient import AsyncHTTPClient

from jsonrpc.dispatcher import JsonRpcDispatcher
from jsonrpc.handlers.socket import run_server
from jsonrpc.registry import MethodRegistry

registry = MethodRegistry()


# plain ol' synchronous rpc method example
@registry.register("echo")
def echo(*args, **kwargs):
    if args:
        return args
    elif kwargs:
        return kwargs


# async rpc method example
@registry.register("get_homepage")
async def get_homepage():
    http_client = AsyncHTTPClient()
    response = await http_client.fetch("http://tevans.us")
    return response.body


dispatcher = JsonRpcDispatcher(registry=registry)

run_server(dispatcher, host="", port=8000, listen_backlog=128)
```