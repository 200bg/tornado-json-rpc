from tornado.httpclient import AsyncHTTPClient

from jsonrpc.dispatcher import JsonRpcDispatcher
from jsonrpc.handlers.socket import run_server
from jsonrpc.registry import MethodRegistry

registry = MethodRegistry()


# plain ol' synchronous rpc method example
@registry.register("echo")
def echo(*args, **kwargs):
    if args:
        return args
    elif kwargs:
        return kwargs


# async rpc method example
@registry.register("get_homepage")
async def get_homepage():
    http_client = AsyncHTTPClient()
    response = await http_client.fetch("http://tevans.us")
    return response.body


dispatcher = JsonRpcDispatcher(registry=registry)

run_server(dispatcher, host="", port=8000, listen_backlog=128)
