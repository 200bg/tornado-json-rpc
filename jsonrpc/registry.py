from logging import getLogger

from jsonrpc.exc import AlreadyRegisteredError, JsonRpcMethodNotFound

logger = getLogger(__name__)


class MethodRegistry:
    def __init__(self, methods=None):
        self._methods = methods or {}

    def register(self, namespace):
        def decorator(func):
            self.register_method(namespace, func)
            return func

        return decorator

    def register_method(self, namespace, method):
        #print('{}:   {}'.format(namespace, method.__module__))
        if namespace in self._methods:
            raise AlreadyRegisteredError("Method already registered at namespace %s.", namespace)
        self._methods[namespace] = method

    def deregister_method(self, namespace):
        if namespace in self._methods:
            del self._methods[namespace]

    def lookup(self, request):
        try:
            method = self[request.method]
        except KeyError as exc:
            logger.exception(exc)
            raise JsonRpcMethodNotFound()
        return method

    def __getitem__(self, item):
        return self._methods[item]
