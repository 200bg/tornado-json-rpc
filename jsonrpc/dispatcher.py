import inspect

from jsonrpc.authentication import AnonymousUser
from jsonrpc.exc import JsonRpcError, JsonRpcInternalError, JsonRpcInvalidParams
from jsonrpc.fields import MissingField
from jsonrpc.handlers.web import logger
from jsonrpc.messaging import JsonRpcRequest, JsonRpcResponse, NullResponse, format_error_response, \
    format_result_response

try:
    import ujson as json
except ImportError:
    import json

async def call_method(method, args=None, kwargs=None):
    args = args or []
    kwargs = kwargs or {}
    is_coroutine = inspect.iscoroutinefunction(method) or inspect.iscoroutine(method)

    if is_coroutine:
        result = await method(*args, **kwargs)
    else:
        result = method(*args, **kwargs)
    return result


class JsonRpcDispatcher:
    """ Handles request dispatching and method calling
    """

    def __init__(self, registry, app_logger=None):
        self._registry = registry
        if app_logger is None:
            self.logger = logger
        else:
            self.logger = app_logger

    async def handle_message(self, message, user=None, _request=None, _client=None):
        """ Takes a serialized json-rpc request, parses it and resolves it.

        This will look the request method up in the registry and call it with the provided
          parameters.

        :param message: a serialized json-rpc request string
        :return: serialized json-rpc response or NullResponse
        """
        request = None
        try:
            request = JsonRpcRequest.from_string(message)
            response = await self.handle_request(request, user=user, _request=_request, _client=_client)
        except JsonRpcError as exc:
            response = format_error_response(None, exc)
            logger.exception(self.format_error_log_msg(request or message, response, user, _client))

        if request and request.id is not MissingField:
            return json.dumps(response)
        return NullResponse

    async def handle_request(self, request, user=None, _request=None, _client=None):
        """ Takes a JsonRpcRequest instance and resolves it.

        This will look the request method up in the registry and call it with the provided
          parameters.

        :param request: a JsonRpcRequest instance
        :return: a json-rpc response dictionary or NullResponse
        """
        registry = self._registry
        try:
            method = registry.lookup(request)
            args, kwargs = request.get_args_kwargs()
            kwargs['_request'] = _request
            kwargs['_client'] = _client
            kwargs['user'] = user
            result = await call_method(method, args, kwargs)
            response = format_result_response(JsonRpcResponse(request.id, result))
            logger.info(self.format_success_log_msg(request, response, user, _client))
        except JsonRpcError as exc:
            response = format_error_response(request.id, exc)
        # except ValidationError as exc:
        #     response = format_error_response(request.id, JsonRpcInvalidParams(data=exc.args))
        #     logger.exception(self.format_error_log_msg(request, response, user, _client))
        except TypeError as exc:
            response = format_error_response(request.id, JsonRpcInvalidParams(data=str(exc)))
            if self.logger:
                request_info = self.format_request_info(_client)
                self.logger.exception(request_info, exc_info=exc)
            else:
                logger.exception(self.format_error_log_msg(request, response, user, _client))
        except Exception as exc:
            response = format_error_response(request.id, JsonRpcInternalError(data=str(exc)))
            if self.logger:
                request_info = self.format_request_info(_client)
                self.logger.exception(request_info, exc_info=exc)
            else:
                logger.exception(self.format_error_log_msg(request, response, user, _client))
        return response

    def format_log_msg(self, label, request, response, user, client):
        try:
            displayable_request = request.to_dict()
        except:
            displayable_request = request
        return '{}: {} response: {} user: {} client: {}'.format(label, displayable_request, response, self.format_user_for_log(user), self.format_client_for_log(client))

    def format_user_for_log(self, user):
        return str(user)

    def format_client_for_log(self, client):
        return str(client)

    def format_success_log_msg(self, request, response, user, client):
        return self.format_log_msg('request succeeded', request, response, user, client)

    def format_error_log_msg(self, request, response, user, client):
        return self.format_log_msg('request failed', request, response, user, client)

    def format_request_info(self, _client):
        headers_list = ['    {}: {}'.format(key, val) for key, val in _client.request.headers.items()]
        headers = '\n'.join(headers_list)
        return 'Request:\n\n'\
               'Remote Address: {} \n\n' \
               'Method: {} \n\n' \
               'Hostname: {} \n\n' \
               'URL: {} \n\n' \
               'Path: {} \n\n' \
               'Query: {} \n\n' \
               'Headers: \n{}' \
               'Body: {} \n\n'.format(
            _client.request.remote_ip,
            _client.request.method,
            _client.request.host,
            _client.request.uri,
            _client.request.path,
            _client.request.query,
            headers,
            _client.request.body,
        )
