import asyncio
import errno
import functools
import socket
from logging import getLogger

from tornado import iostream
from tornado.platform.asyncio import AsyncIOMainLoop

from jsonrpc.authentication import AnonymousUser, AuthError
from jsonrpc.messaging import NullResponse

try:
    import ujson as json
except ImportError:
    import json

logger = getLogger(__name__)


class JsonRpcSocketConnectionInfo:
    """This is where any information about the actual connection can be gleaned. Most importantly remote_ip."""
    connection = None
    remote_ip = None

    def __init__(self, connection, remote_ip):
        self.connection = connection
        self.remote_ip = remote_ip


class JsonRpcSocketHandler:
    current_user = None
    connection_info = None

    def __init__(self, connection_info, dispatcher, authenticator):
        self.connection_info = connection_info
        self.stream = iostream.IOStream(self.connection_info.connection)
        self.dispatcher = dispatcher
        self.authenticator = authenticator
        self._is_authenticated = False if authenticator else True

        self._read()

    async def set_current_user(self, data):
        if self.authenticator:
            self.current_user = await self.authenticator.get_user(data)
        else:
            self.current_user = AnonymousUser

    def _read(self):
        self.stream.read_until(b'\r\n', self._eol_callback)

    def _eol_callback(self, data):
        self.handle_data(data)

    async def handle_data(self, data):
        if not self._is_authenticated:
            try:
                await self.set_current_user(data)
                self._is_authenticated = True
            except AuthError:
                self.stream.write("Unauthorized")
                self.connection_info.connection.close()

        result = await self.dispatcher.handle_message(data, self.current_user, _request=self.connection_info, _client=self)
        if result is not NullResponse:
            self.stream.write(result)
        self._read()


def connection_ready(sock, method_registry, fd, events):
    while True:
        try:
            connection, address = sock.accept()
        except socket.error as exc:
            if exc.args[0] not in (errno.EWOULDBLOCK, errno.EAGAIN):
                raise
            return
        connection.setblocking(0)
        connection_info = JsonRpcSocketConnectionInfo(connection, address)
        JsonRpcSocketHandler(connection_info, method_registry)


def run_server(registry, host="", port=8000, listen_backlog=128):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.setblocking(0)
    sock.bind((host, port))
    sock.listen(listen_backlog)

    AsyncIOMainLoop().install()
    io_loop = asyncio.get_event_loop()

    callback = functools.partial(connection_ready, sock, registry)
    io_loop.add_handler(sock.fileno(), callback, io_loop.READ)

    try:
        io_loop.start()
    except KeyboardInterrupt:
        io_loop.stop()
