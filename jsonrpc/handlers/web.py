from logging import getLogger

from tornado.escape import utf8
from tornado.web import RequestHandler

from jsonrpc.authentication import AnonymousUser, AuthError
from jsonrpc.messaging import NullResponse

try:
    import ujson as json
except ImportError:
    import json

logger = getLogger(__name__)


class JsonRpcRequestHandler(RequestHandler):
    SUPPORTED_METHODS = ['POST']

    def initialize(self, dispatcher, authenticator=None):
        super().initialize()
        self.dispatcher = dispatcher
        self.authenticator = authenticator


    async def set_current_user(self):
        if self.authenticator:
            self._current_user = await self.authenticator.get_user(self.request)
        else:
            self._current_user = AnonymousUser

    async def post(self, *args, **kwargs):
        try:
            await self.set_current_user()
        except AuthError as exc:
            self.send_error(401)
            return

        result = await self.dispatcher.handle_message(self.request.body, self.current_user,  _request=self.request, _client=self)
        if result is not NullResponse:
            self.write(result)

    def write(self, chunk):
        if self._finished:
            raise RuntimeError("Cannot write() after finish()")
        # if isinstance(chunk, dict):
        #     chunk = escape.json_encode(chunk)
        self.set_header("Content-Type", "application/json; charset=UTF-8")
        chunk = utf8(chunk)
        self._write_buffer.append(chunk)
