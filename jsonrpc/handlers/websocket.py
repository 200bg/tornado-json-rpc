from logging import getLogger

from tornado import gen
from tornado.websocket import WebSocketHandler

from jsonrpc.authentication import AnonymousUser, AuthError
from jsonrpc.messaging import NullResponse

try:
    import ujson as json
except ImportError:
    import json

logger = getLogger(__name__)


class JsonRpcWebSocketHandler(WebSocketHandler):
    def initialize(self, dispatcher, authenticator=None):
        super().initialize()
        self.dispatcher = dispatcher
        self.authenticator = authenticator

    async def set_current_user(self):
        if self.authenticator:
            self._current_user = await self.authenticator.get_user(self.request)
        else:
            self._current_user = AnonymousUser

    @gen.coroutine
    def on_message(self, message):
        try:
            yield from self.set_current_user()
        except AuthError:
            logger.exception('request authorization failed: {}'.format(message))
            self.close(401)
            return

        result = yield from self.dispatcher.handle_message(message, self.current_user, _client=self, _request=self.request)
        if result is not NullResponse:
            self.write_message(result)
