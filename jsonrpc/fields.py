class MissingField:
    """ Represents a field that was not included in a json-rpc request or response

    This value should be used for missing fields instead of None, as some omittable fields
      can have null values when present.
    """
    pass