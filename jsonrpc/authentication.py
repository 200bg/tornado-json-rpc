class AnonymousUser:
    pass


class AuthError(Exception):
    pass


class Authenticator:
    async def get_user(self, request):
        """ Called by the request handlers to get the currently logged in user.
            Raises AuthError if there are any problems.

        :param request: The request for the transport/protocol
        :return: The user specified in the request
        """
        raise NotImplementedError

