from jsonrpc.fields import MissingField


class JsonRpcError(Exception):
    """ A json-rpc 2.0 error response object.

    Provides serialization and validation methods
    """
    version = '2.0'
    do_validation = True

    def __init__(self, code, message, data=MissingField):
        super().__init__(code, message, data)
        self.code = code
        self.message = message
        self.data = data
        if self.do_validation:
            self.validate()

    def validate(self):
        if not isinstance(self.code, int):
            raise JsonRpcInternalError(data='"code" field must be an Integer.')

    @classmethod
    def from_dict(cls, d):
        error = d['error']
        return cls(
                error['code'],
                error['message'],
                error.get('data', MissingField)
        )

    def to_dict(self):
        error = {
            'code': self.code,
            'message': self.message,
        }
        if self.data is not MissingField:
            error['data'] = self.data

        return {
            'jsonrpc': self.version,
            'error': error
        }


class JsonRpcParseError(JsonRpcError):
    def __init__(self, code=-32700, message=None, data=MissingField):
        super().__init__(code, 'Parse Error', data)


class JsonRpcInvalidRequest(JsonRpcError):
    def __init__(self, code=-32600, message=None, data=MissingField):
        super().__init__(code, 'Invalid request', data)


class JsonRpcMethodNotFound(JsonRpcError):
    def __init__(self, code=-32601, message=None, data=MissingField):
        super().__init__(code, 'Method not found', data)


class JsonRpcInvalidParams(JsonRpcError):
    def __init__(self, code=-32602, message=None, data=MissingField):
        super().__init__(code, 'Invalid params', data)


class JsonRpcInternalError(JsonRpcError):
    def __init__(self, code=-32603, message=None, data=MissingField):
        super().__init__(code, 'Internal error', data)


class AlreadyRegisteredError(Exception):
    pass
