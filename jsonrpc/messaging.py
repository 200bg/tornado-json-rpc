from jsonrpc.exc import JsonRpcError, JsonRpcParseError, JsonRpcInvalidRequest, JsonRpcMethodNotFound, \
    JsonRpcInvalidParams, JsonRpcInternalError
from jsonrpc.fields import MissingField

try:
    import ujson as json
except ImportError:
    import json


class JsonRpcRequest:
    """ A json-rpc 2.0 request object.

    Provides serialization, de-serialization, and validation of request objects
    """
    version = '2.0'

    def __init__(self, method, params=MissingField, _id=MissingField, version=MissingField, do_validation=True):
        self.method = method
        self.params = params
        self.id = _id

        if do_validation:
            self.validate()

    def validate(self):
        if not isinstance(self.version, str):
            raise JsonRpcInvalidRequest(self.id, data='"jsonrpc" field must be a string.')

        if not isinstance(self.method, str):
            raise JsonRpcInvalidRequest(self.id, data='"method" field must be a string.')

        if self.params is not MissingField \
                and not isinstance(self.params, list) \
                and not isinstance(self.params, tuple) \
                and not isinstance(self.params, dict):
            raise JsonRpcInvalidRequest(data='"params" field must be an object or array or omitted.')

        if self.id is not MissingField \
                and not isinstance(self.id, int) \
                and not isinstance(self.id, str) \
                and self.id is not None:
            raise JsonRpcInvalidRequest(data='"id" field must be an Integer, String, or null.')

    def get_args_kwargs(self):
        """ Takes the value of the JsonR

        :param params: the 'params' field from a json-rpc request. should be a list, dict, or MissingField
        :return: (list, dict) for use as args and kwargs, respectively
        """
        params = self.params
        if params is MissingField:
            return [], {}
        elif isinstance(params, dict):
            return [], params
        elif isinstance(params, list) or isinstance(params, tuple):
            return params, {}
        raise JsonRpcInvalidParams()

    def to_dict(self):
        result = {
            'jsonrpc': self.version,
            'method': self.method,
        }
        if self.params is not MissingField:
            result['params'] = self.params
        if self.id is not MissingField:
            result['id'] = self.id

        return result

    @classmethod
    def from_dict(cls, d):
        return cls(
                d.get('method'),
                d.get('params', MissingField),
                d.get('id', MissingField)
        )

    @classmethod
    def from_string(cls, request_string):
        if len(request_string) == 0:
            raise JsonRpcParseError(data='Empty request.')
        try:
            if isinstance(request_string, bytes):
                return cls.from_dict(json.loads(request_string.decode("utf-8")))
            else:
                return cls.from_dict(json.loads(request_string))
        except ValueError:
            raise JsonRpcParseError()

    @classmethod
    def from_fd(cls, fd):
        try:
            return cls.from_dict(json.load(fd))
        except ValueError:
            raise JsonRpcParseError()


class JsonRpcResponse:
    """ A json-rpc 2.0 response object.

    Provides serialization and validation methods
    """
    version = '2.0'
    _code_class_mapping = {
        -32700: JsonRpcParseError,
        -32600: JsonRpcInvalidRequest,
        -32601: JsonRpcMethodNotFound,
        -32602: JsonRpcInvalidParams,
        -32603: JsonRpcInternalError
    }
    do_validation = True

    def __init__(self, _id, result):
        self.id = _id
        self.result = result

        if self.do_validation:
            self.validate()

    def validate(self):
        if not isinstance(self.id, int) and not isinstance(self.id, str) and self.id is not None:
            raise JsonRpcInternalError(self.id, data='"id" field must be an Integer, String, or null.')

    def to_dict(self):
        return {
            'jsonrpc': self.version,
            'result': self.result,
            'id': self.id
        }

    @classmethod
    def from_dict(cls, d):
        if 'error' in d:
            error = d['error']
            code_cls = cls._code_class_mapping.get(error['code'])
            if code_cls:
                raise code_cls(error['code'], error['message'], error.get('data', MissingField))
            raise JsonRpcError.from_dict(d)
        return JsonRpcResponse(d.get('id'), d.get('result'))

    @classmethod
    def from_string(cls, response_string):
        return cls.from_dict(json.loads(response_string))

    @classmethod
    def from_fd(cls, fd):
        return cls.from_dict(json.load(fd))


class NullResponse:
    """ Returned by message dispatchers to signify that no response should be made.

    Used if the request was an announcement
    """
    pass


def format_error_response(request_id, error):
    result = error.to_dict()
    result['id'] = request_id
    return result


def format_result_response(result):
    return result.to_dict()
