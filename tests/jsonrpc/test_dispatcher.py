import asyncio
from unittest import TestCase

from jsonrpc.dispatcher import JsonRpcDispatcher, call_method
from jsonrpc.messaging import JsonRpcRequest, JsonRpcResponse
from jsonrpc.registry import MethodRegistry

loop = asyncio.get_event_loop()

def echo(*args, **kwargs):
    if args:
        return args
    elif kwargs:
        return kwargs


class TestJsonRpcDispatcher(TestCase):

    def test_sync_method(self):
        def foo():
            self.assertTrue(True)

        async def _():
            await call_method(foo)
        loop.run_until_complete(_())

    def test_async_method(self):
        async def foo():
            self.assertTrue(True)

        async def _():
            await call_method(foo)
        loop.run_until_complete(_())

    def test_passing_args(self):
        def foo(a, b):
            self.assertTrue(a == 1 and b == 2)

        async def _():
            await call_method(foo, [1, 2])
        loop.run_until_complete(_())

    def test_passing_kwargs(self):
        def foo(a, b):
            self.assertTrue(a == 1 and b == 2)

        async def _():
            await call_method(foo, kwargs={"a":1, "b":2})
        loop.run_until_complete(_())

    def test_dispatch(self):
        request = JsonRpcRequest("echo", (1, 2, 3), 1)
        registry = MethodRegistry({"echo": echo})
        dispatcher = JsonRpcDispatcher(registry)

        async def _():
            response = await dispatcher.handle_request(request)
            reference_response = JsonRpcResponse(1, (1, 2, 3)).to_dict()
            self.assertEqual(response, reference_response)

        loop.run_until_complete(_())

    def test_dispatch_object_params(self):
        request = JsonRpcRequest("echo", {"a": 1, "b": 2}, 1)
        registry = MethodRegistry({"echo": echo})
        dispatcher = JsonRpcDispatcher(registry)

        async def _():
            response = await dispatcher.handle_request(request)
            reference_response = JsonRpcResponse(1, {"a": 1, "b": 2}).to_dict()
            self.assertEqual(response, reference_response)

        loop.run_until_complete(_())

    def test_dispatch_string_params(self):
        request = JsonRpcRequest("echo", "this_should_be_a_list", 1, do_validation=False)
        registry = MethodRegistry({"echo": echo})
        dispatcher = JsonRpcDispatcher(registry)

        async def _():
            response = await dispatcher.handle_request(request)
            self.assertEqual(response, {'jsonrpc': '2.0', 'id': 1, 'error': {'code': -32602, 'message': 'Invalid params'}})

        loop.run_until_complete(_())
