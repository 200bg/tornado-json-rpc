from unittest import TestCase

from jsonrpc.exc import JsonRpcInvalidRequest, JsonRpcError, JsonRpcMethodNotFound, \
    JsonRpcInternalError, JsonRpcInvalidParams, JsonRpcParseError
from jsonrpc.messaging import JsonRpcRequest, JsonRpcResponse


class TestJsonRpcRequest(TestCase):
    def test_basic(self):
        request = JsonRpcRequest('foo', [], 1)
        d = request.to_dict()
        self.assertEqual(d, {'method': 'foo', 'params': [], 'jsonrpc': '2.0', 'id': 1})

    def test_announce_message(self):
        request = JsonRpcRequest('foo', [])
        d = request.to_dict()
        self.assertEqual(d, {'method': 'foo', 'params': [], 'jsonrpc': '2.0'})

    def test_id_null(self):
        request = JsonRpcRequest('foo', [], None)
        d = request.to_dict()
        self.assertEqual(d, {'method': 'foo', 'params': [], 'jsonrpc': '2.0', 'id': None})

    def test_missing_params(self):
        request = JsonRpcRequest('foo', _id=1)
        d = request.to_dict()
        self.assertEqual(d, {'method': 'foo', 'jsonrpc': '2.0', 'id': 1})

    def test_from_dict(self):
        request = JsonRpcRequest.from_dict(
                {'method': 'foo', 'params': [], 'jsonrpc': '2.0', 'id': 1}
        )
        d = request.to_dict()
        self.assertEqual(d, {'method': 'foo', 'params': [], 'jsonrpc': '2.0', 'id': 1})

    def test_from_string(self):
        request = JsonRpcRequest.from_string(
                '{"id": 1, "method": "foo", "params": [], "jsonrpc": "2.0"}'
        )
        d = request.to_dict()
        self.assertEqual(d, {'method': 'foo', 'params': [], 'jsonrpc': '2.0', 'id': 1})

    def test_missing_method_from_string(self):
        with self.assertRaises(JsonRpcInvalidRequest):
            JsonRpcRequest.from_string(
                    '{"id": 1, "params": [], "jsonrpc": "2.0"}'
            )

    def test_nonstring_method(self):
        with self.assertRaises(JsonRpcInvalidRequest):
            JsonRpcRequest({'this': 'should_be_a_string'}, [], 1)

    def test_string_params(self):
        with self.assertRaises(JsonRpcInvalidRequest):
            JsonRpcRequest('foo', 'this_shouldnt_be_a_string', 1)

    def test_invalid_id(self):
        with self.assertRaises(JsonRpcInvalidRequest):
            JsonRpcRequest('foo', [], [])

    def test_bypass_validation(self):
        JsonRpcRequest('foo', [], None, do_validation=False)


class TestJsonRpcResponse(TestCase):
    def test_basic(self):
        response = JsonRpcResponse(1, 1)
        d = response.to_dict()
        self.assertEqual(d, {'jsonrpc': '2.0', 'result': 1, 'id': 1})

    def test_invalid_id(self):
        with self.assertRaises(JsonRpcInternalError):
            JsonRpcResponse([], 1)

    def test_bypass_validation(self):
        JsonRpcResponse([], 1, do_validation=False)

    def test_from_dict(self):
        response = JsonRpcResponse.from_dict(
                {'result': 1, 'id': 1, 'jsonrpc': '2.0'}
        )
        self.assertIsInstance(response, JsonRpcResponse)

    def test_generic_error(self):
        with self.assertRaises(JsonRpcError):
            JsonRpcResponse.from_dict(
                    {
                        'error': {
                            'code': 1,
                            'message': 'Foo',
                        },
                        'id': 1
                    }
            )

    def test_parse_error(self):
        with self.assertRaises(JsonRpcParseError):
            JsonRpcResponse.from_dict(
                    {
                        'error': {
                            'code': -32700,
                            'message': 'Foo',
                        },
                        'id': 1
                    }
            )

    def test_invalid_request(self):
        with self.assertRaises(JsonRpcInvalidRequest):
            JsonRpcResponse.from_dict(
                    {
                        'error': {
                            'code': -32600,
                            'message': 'Foo',
                        },
                        'id': 1
                    }
            )

    def test_method_not_found(self):
        with self.assertRaises(JsonRpcMethodNotFound):
            JsonRpcResponse.from_dict(
                    {
                        'error': {
                            'code': -32601,
                            'message': 'Foo',
                        },
                        'id': 1
                    }
            )

    def test_invalid_params(self):
        with self.assertRaises(JsonRpcInvalidParams):
            JsonRpcResponse.from_dict(
                    {
                        'error': {
                            'code': -32602,
                            'message': 'Foo',
                        },
                        'id': 1
                    }
            )

    def test_internal_error(self):
        with self.assertRaises(JsonRpcInternalError):
            JsonRpcResponse.from_dict(
                    {
                        'error': {
                            'code': -32603,
                            'message': 'Foo',
                        },
                        'id': 1
                    }
            )

    def parse_bad_error(self):
        with self.assertRaises(JsonRpcInternalError):
            JsonRpcResponse.from_dict(
                    {
                        'error': {
                            'code': 'this_should_be_an_int',
                            'message': 'Foo',
                        },
                        'id': 1
                    }
            )

    def parse_bad_result(self):
        with self.assertRaises(JsonRpcInternalError):
            JsonRpcResponse.from_dict(
                    {'result': 1, 'id': {}}
            )
