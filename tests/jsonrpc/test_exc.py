from unittest import TestCase

from jsonrpc.exc import JsonRpcError, JsonRpcInternalError


class TestJsonRpcError(TestCase):
    def test_basic(self):
        error = JsonRpcError(-32700, 'Parse error', 'The request could not be parsed')
        d = error.to_dict()
        self.assertEqual(d, {
            'error': {
                'code': -32700,
                'message': 'Parse error',
                'data': 'The request could not be parsed'
            },
            'jsonrpc': '2.0'
        })

    def test_nonint_code(self):
        with self.assertRaises(JsonRpcInternalError):
            JsonRpcError('this_should_be_an_int', 'Parse error', 'The request could not be parsed')

    def test_invalid_id(self):
        with self.assertRaises(JsonRpcInternalError):
            JsonRpcError([], -32700, 'Parse error', 'The request could not be parsed')

    def test_bypass_validation(self):
        JsonRpcError('this_should_be_an_int', 'Parse error', 'The request could not be parsed', do_validation=False)
