import asyncio
from unittest import TestCase

from jsonrpc.dispatcher import JsonRpcDispatcher
from jsonrpc.exc import JsonRpcMethodNotFound
from jsonrpc.messaging import JsonRpcRequest, JsonRpcResponse
from jsonrpc.registry import MethodRegistry

loop = asyncio.get_event_loop()


def echo(*args, **kwargs):
    if args:
        return args
    elif kwargs:
        return kwargs


class TestMethodRegistry(TestCase):
    def test_basic_initilization(self):
        registry = MethodRegistry({"echo": echo})
        self.assertEqual(registry['echo'], echo)

    def test_deferred_initialization(self):
        registry = MethodRegistry()
        registry.register_method("echo", echo)
        self.assertEqual(registry['echo'], echo)
        method = registry['echo']
        self.assertEqual(method(1), (1,))

    def test_lookup(self):
        registry = MethodRegistry({"echo": echo})
        request = JsonRpcRequest("echo", {"a": 1, "b": 2}, 1)
        self.assertEqual(registry.lookup(request), echo)

    def test_lookup_unknown_method(self):
        registry = MethodRegistry()
        request = JsonRpcRequest("echo", {"a": 1, "b": 2}, 1)
        with self.assertRaises(JsonRpcMethodNotFound):
            registry.lookup(request)

    def test_getitem_unknown_method(self):
        registry = MethodRegistry()
        with self.assertRaises(KeyError):
            registry['echo']

    def test_decorator(self):
        async def _():
            registry = MethodRegistry()

            @registry.register('local_echo')
            def local_echo(*args, **kwargs):
                if args:
                    return args
                elif kwargs:
                    return kwargs

            dispatcher = JsonRpcDispatcher(registry)
            request = JsonRpcRequest("local_echo", {"a": 1, "b": 2}, 1)
            response = await dispatcher.handle_request(request)
            reference_response = JsonRpcResponse(1, {"a": 1, "b": 2}).to_dict()
            self.assertEqual(response, reference_response)

        loop.run_until_complete(_())

    def test_unregister_method(self):
        registry = MethodRegistry({"echo": echo})
        registry.deregister_method("echo")
        with self.assertRaises(KeyError):
            registry['echo']
