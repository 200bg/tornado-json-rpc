#!/usr/bin/env python
import os
from setuptools import setup, find_packages
from jsonrpc import VERSION


def get_requirements():
    req_file = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'REQUIREMENTS')
    rfh = open(req_file, 'r')
    requirements = rfh.read().split('\n')
    app_requirements = [i for i in requirements if not i.startswith('#') and i.find('://') == -1]
    requirements = app_requirements
    return requirements


def get_dependencies():
    req_file = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'REQUIREMENTS')
    rfh = open(req_file, 'r')
    dependency_links = rfh.read().split('\n')
    dependency_links  = [i for i in dependency_links if not i.startswith('#') and i.find('://') > 0]
    return dependency_links

readme_file = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'README.md')

setup(
    name='tornado-json-rpc',
    version=VERSION,
    packages=find_packages(),
    install_requires=get_requirements(),
    author='Tyler A Evans',
    author_email='tyler@tevans.us',
    description="json-rpc server built on top of tornado",
    long_description=open(readme_file).read(),
    setup_requires=['setuptools',],
    url='https://bitbucket.org/baddad/tornado-json-rpc',
    license='Apache-2.0',
)
